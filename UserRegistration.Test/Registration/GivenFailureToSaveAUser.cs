﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using UserRegistration.Interfaces;

namespace UserRegistration.Test.Registration
{
    [TestClass]
    public class GivenFailureToSaveAUser
    {
        [TestMethod]
        public void UserRegistrationReturnsFalse()
        {
            //arrange
            var validEmail = "andrew@gmail.com";
            var validPassword = "Password";

            var userModel = new User
            {
                UserName = validEmail,
                Password = validPassword
            };

            var stringHasher = Mock.Of<IStringHasher>();
            var userRepo = Mock.Of<IUserRepository>();
            var userRegistrationService = new UserRegistarionService(stringHasher, userRepo);
            //setup failure of save to database
            Mock.Get(userRepo).Setup(repo => repo.AddUser(It.IsAny<User>())).Returns(false);
            //act

            var result = userRegistrationService.RegisterUser(userModel);
        
            //assert

            Assert.IsFalse(result);
        }
    }
}
