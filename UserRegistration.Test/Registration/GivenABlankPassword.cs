﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using UserRegistration.Interfaces;

namespace UserRegistration.Test.Registration
{   
    [TestClass]
    public class GivenABlankPassword
    {
        [TestMethod]
        public void UserRegistrationFailsOnBlankPassword()
        {
            //arrange
            var validEmail = "bob@gmail.com";
            var user = new User
            {
                UserName = validEmail,
                Password = ""
            };
            var stringHasher = Mock.Of<IStringHasher>();
            var userRepo = Mock.Of<IUserRepository>();

            var userRegistrationService = new UserRegistarionService(stringHasher, userRepo);
            //act

            var result = userRegistrationService.RegisterUser(user);
            //assert

            Assert.IsFalse(result);
        }
    }
}
