﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using UserRegistration.Interfaces;

namespace UserRegistration.Test.Registration
{
    [TestClass]
    public class GivenAValidUserModel
    {
        private string _validEmail;
        private string _validPassword;
        private string _hashedPassword;

        private IStringHasher _stringHasher;
        private IUserRepository _userRepository;


        [TestInitialize]
        public void Setup()
        {
            _validEmail = "bob@gmail.com";
            _validPassword = "BobsPassword";
            _hashedPassword = "This is a hashed password";

            _stringHasher = Mock.Of<IStringHasher>();
            _userRepository = Mock.Of<IUserRepository>();
        }

        [TestMethod]
        public void StringHasherIsCalledWithPassword()
        {
            //arrange
            var userModel = new User
            {
                UserName = _validEmail,
                Password = _validPassword
            };

            var userRegistrationService = new UserRegistarionService(_stringHasher, _userRepository);
            //act
            userRegistrationService.RegisterUser(userModel);

            //assert
            Mock.Get(_stringHasher).Verify(hasher => hasher.HashPassword(_validPassword));
        }
        [TestMethod]
        public void UserRepositoryAddUserIsCalled()
        {
            //arrange
            var userModel = new User
            {
                UserName = _validEmail,
                Password = _validPassword
            };

            var userRegistrationService = new UserRegistarionService(_stringHasher, _userRepository);
            //act

            userRegistrationService.RegisterUser(userModel);
            //assert
            Mock.Get(_userRepository).Verify(userRepo => userRepo.AddUser(userModel));
        }
        [TestMethod]
        public void HashedPasswordIsAssignedToUserModel()
        {
            //arrange
            var userModel = new User
            {
                UserName = _validEmail,
                Password = _validPassword
            };

            var userRegistrationService = new UserRegistarionService(_stringHasher, _userRepository);
            //setup return of hash result
            Mock.Get(_stringHasher).Setup(hasher => hasher.HashPassword(userModel.Password)).Returns(_hashedPassword);
            //act

            userRegistrationService.RegisterUser(userModel);
            //assert

            Assert.AreEqual(userModel.Password, _hashedPassword);
        }
    }
}
