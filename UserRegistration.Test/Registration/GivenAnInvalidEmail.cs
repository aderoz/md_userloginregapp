﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using UserRegistration.Interfaces;

namespace UserRegistration.Test.Registration
{
    [TestClass]
    public class GivenAnInvalidEmail
    {
        [TestMethod]
        public void UserRegistrationServiceRegisterReturnsFalse()
        {
            //arrange
            var invalidEmail = "this is no email";

            var userWithInvalidEmail = new User
            {
                Password = "",
                UserName = invalidEmail
            };

            var stringHasher = Mock.Of<IStringHasher>();
            var userRepo = Mock.Of<IUserRepository>();

            var userRegistrationService = new UserRegistarionService(stringHasher, userRepo);
            //act
            var validationResult = userRegistrationService.RegisterUser(userWithInvalidEmail);
            //assert

            Assert.IsFalse(validationResult);
        }
    }
}
