﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UserRegistration.Test.Registration
{
    [TestClass]
    public class GivenAValidEmail
    {
        [TestMethod]
        public void StringValidatorValidateEmailReturnsTrue()
        {
            //arrange
            var stringValidator = new StringValidator();

            var validEmail = "andrew@andrewmail.com";
            
            //act
            var result = stringValidator.ValidateEmail(validEmail);
            //assert
            Assert.IsTrue(result);
        }
    }
}
