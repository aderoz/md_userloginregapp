﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using UserRegistration.Interfaces;

namespace UserRegistration.Test.Login
{
    [TestClass]
    public class GivenALoginAttempt
    {
        private User _userModel;

        private IUserRepository _userRepository;

        [TestInitialize]
        public void Setup()
        {
            _userModel = new User
            {
                UserName = "testUserName",
                Password = "testPassword"
            };

            _userRepository = Mock.Of<IUserRepository>();
        }
        [TestMethod]
        public void LoginServiceCallsUserRepo()
        {
            //arrange

            var userLoginService = new UserLoginService(_userRepository);
            //act

            userLoginService.Login(_userModel);
            //assert

            Mock.Get(_userRepository).Verify(loginServ => loginServ.Login(_userModel));
        }
        [TestMethod]
        public void LoginServiceReturnsTrueOnRetrival()
        {
            //arrange
            //setup successfule save
            Mock.Get(_userRepository).Setup(userRepo => userRepo.Login(_userModel)).Returns(true);

            var userLoginService = new UserLoginService(_userRepository);
            //act

            var result = userLoginService.Login(_userModel);
            //assert

            Assert.IsTrue(result);
        }
        [TestMethod]
        public void LoginServiceReturnsFalseOnRetreiveFail()
        {
            //arrange
            //setup failure to save
            Mock.Get(_userRepository).Setup(userRepo => userRepo.Login(_userModel)).Returns(false);

            var userLoginService = new UserLoginService(_userRepository);
            //act

            var result = userLoginService.Login(_userModel);
            //assert

            Assert.IsFalse(result);

        }
    }
}
