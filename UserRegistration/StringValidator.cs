﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace UserRegistration
{
    public class StringValidator
    {
        private string _emailRegex = @"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$"; 

        public bool ValidateEmail(string emailStringToValidate)
        {
            var validationResult = IsStringEmail(emailStringToValidate);

            return validationResult;
        }

        private bool IsStringEmail(string email)
        {
            Regex regex = new Regex(_emailRegex);
            Match match = regex.Match(email);

            var wasEmail = match.Success;

            return wasEmail;
        }
    }
}
