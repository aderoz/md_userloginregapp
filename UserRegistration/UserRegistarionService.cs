﻿using System;
using System.Collections.Generic;
using System.Linq;
using UserRegistration.Interfaces;


namespace UserRegistration
{
    public class UserRegistarionService
    {
        private StringValidator _stringValidator;
        private IStringHasher _stringHasher;
        private IUserRepository _userRepository;

        public UserRegistarionService(IStringHasher stringHasher, IUserRepository userRepository)
        {
            _stringHasher = stringHasher;
            _userRepository = userRepository;
            _stringValidator = new StringValidator();
        }

        public bool RegisterUser(User userModel)
        {
            var userNameIsEmail = _stringValidator.ValidateEmail(userModel.UserName);

            if (!userNameIsEmail)
                return false;

            if(string.IsNullOrEmpty(userModel.Password))
                return false;
            
            var hashedPassword = _stringHasher.HashPassword(userModel.Password);

            userModel.Password = hashedPassword;

            var result = _userRepository.AddUser(userModel);

            return result;
        }


    }
}
