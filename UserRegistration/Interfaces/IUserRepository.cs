﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserRegistration.Interfaces
{
    public interface IUserRepository
    {
        bool AddUser(User newUser);
        bool Login(User userToAuthenticate);
    }
}
