﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserRegistration.Interfaces
{
    public interface IStringHasher
    {
        string HashPassword(string unEncryptedPassword);
    }
}
