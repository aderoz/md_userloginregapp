﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserRegistration.Interfaces;

namespace UserRegistration
{
    public class UserLoginService
    {
        private IUserRepository _userRepo;

        public UserLoginService(IUserRepository userRepository)
        {
            _userRepo = userRepository;
        }

        public bool Login(User userLoggingIn)
        {
            var loginResult = _userRepo.Login(userLoggingIn);

            return loginResult;
        }
    }
}
